package curp;
/**
 * En esta clase deben de terminar de implementar los metodos,
 * pueden definir metodos auxiliares si lo consideran necesario.
 * No deben de modificar la firma de ningun metodo.
 * @author diego
 */
public class CURP {
    /*
    * Método que limpia las cadenas de acentos 
    * Considerando que solo las vocales llevan acentos
    * También cambia las letras Ñ que se llegasen a econtrar por X
    *@param cadena (nombre, aprellidos)
    *@return La cadena sin acentos.
    */
    String sinAcentos(String cadena){
        cadena = cadena.trim().toUpperCase();//Nos quedamos con la cadena ya en mayúsculas
        cadena = cadena.replace("Á","A");
        cadena = cadena.replace("É","E");
        cadena = cadena.replace("Í","I");
        cadena = cadena.replace("Ó","O");
        cadena = cadena.replace("Ú","U");
        cadena = cadena.replace("Ñ","X");
        return cadena;
    }
    
    /**
     * Metodo que regresa el primer caracter del nombre (Con el formato adecuado).
     * Deben considerar que el curp no lleva acentos.
     * @param nombre Nombre(s) del usuario.
     * @return El primer caracter del nombre.
     */
    String nombre(String nombre){
        nombre = sinAcentos(nombre);
        return nombre.substring(0,1);
    }
    
    /**
     * Metodo que regresa los primeros dos caracteres del apellido paterno (Con el formato adecuado).
     * Deben considerar que el curp no lleva acentos.
     * @param apellido_p El apellido paterno del usuario.
     * @return Los dos primero caracteres del apellido paterno.
     */
    String apellidoPaterno(String apellido_p){
        apellido_p = sinAcentos(apellido_p);
        String x = apellido_p.substring(0,1);
        char vocal = ' ';
        int i=1;
        while(vocal == ' '){
            switch(apellido_p.charAt(i)){
                    case 'A':
                        vocal = 'A';
                        break;
                    case 'E':
                        vocal = 'E';
                        break;
                    case 'I':
                        vocal = 'I';
                        break;
                    case 'O':
                        vocal = 'O';
                        break;
                    case 'U':
                        vocal = 'U';
                        break;
                    default:
                        i++;
                        break;
                }
        }
        return x.concat(Character.toString(vocal));
    }
    
    /**
     * Metodo que regresa el primer caracter del apellido materno (Con el formato adecuado).
     * Deben considerar que el curp no lleva acentos.
     * @param apellido_m El apellido materno del usuario.
     * @return El primer caracter del apellido materno.
     */
    String apellidoMaterno(String apellido_m){
        apellido_m = sinAcentos(apellido_m);
        return apellido_m.substring(0,1);
    }
    
    /**
     * Metodo que regresa el mes en formato: mm.
     * El metodo debe de aceptar el nombre del mes sin importar si vienen en minusculas o
     * mayusculas o mixto, tambien debe de aceptar si el mes fue dado como un entero.
     * Ejemplos de posibles entradas:
     * - Enero
     * - enero
     * - EnErO
     * - 01
     * - 1
     * Para cualquiera de las entradas anteriores la cadena que debe regresar es: 01
     * @param mes El mes dado.
     * @return El mes en formato: mm.
     */
    String getMes(String mes){
        String mm = "00";
        mes = mes.trim().toLowerCase();
                switch(mes){
                    case "enero": 
                        mm = "01";
                        break;
                    case "febrero":
                        mm = "02";
                        break;
                    case "marzo":
                        mm = "03";
                        break;
                    case "abril":
                        mm = "04";
                        break;
                    case "mayo":
                        mm = "05";
                        break;
                    case "junio":
                        mm = "06";
                        break;
                    case "julio":
                        mm = "07";
                        break;
                    case "agosto":
                        mm = "08";
                        break;
                    case "septiembre":
                        mm = "09";
                        break;
                    case "octubre":
                        mm = "10";
                        break;
                    case "noviembre":
                        mm = "11";
                        break;
                    case "diciembre":
                        mm = "12";
                        break;
                    case "1": 
                        mm = "01";
                        break;
                    case "01": 
                        mm = "01";
                        break;
                    case "02":
                        mm = "02";
                        break;
                    case "3":
                        mm = "03";
                        break;
                    case "03":
                        mm = "03";
                        break;
                    case "4":
                        mm = "04";
                        break;
                    case "04":
                        mm = "04";
                        break;    
                    case "5":
                        mm = "05";
                        break;
                    case "05":
                        mm = "05";
                        break;
                    case "6":
                        mm = "06";
                        break;
                    case "06":
                        mm = "06";
                        break;
                    case "7":
                        mm = "07";
                        break;
                    case "07":
                        mm = "07";
                        break;
                    case "8":
                        mm = "08";
                        break;
                    case "08":
                        mm = "08";
                        break;
                    case "9":
                        mm = "09";
                        break;
                     case "09":
                        mm = "09";
                        break;
                    case "10":
                        mm = "10";
                        break;
                    case "11":
                        mm = "11";
                        break;
                    case "12":
                        mm = "12";
                        break;
                    default:
                            mm = "00";
        }
        return mm;
    }
    
    /**
     * Metodo que regresa la fecha de nacimiento en el formato correcto: aammdd
     * La fecha viene en el siguiente formato: dd/mm/aaaa
     * Hint: Usen el metodo split para obtener los datos.
     * @param fecha La fecha de nacimiento del usuario.
     * @return La fecha en formato: aammdd.
     */
    String fechaNacimiento(String fecha){
        String [] arreglo = fecha.split("/");
        for(int i=0; i<3; i++){
            arreglo[i] = arreglo[i].trim();
        }
        if(arreglo[0].length()<2){
            int d=Integer.parseInt(arreglo[0]);
            if(d>0&&d<10)
                arreglo[0] = "0".concat(arreglo[0]);  
        }
        return arreglo[2].substring(2).concat(getMes(arreglo[1]).concat(arreglo[0]));
    }
    
    /**
     * Metodo que regresa el primer caracter del sexo dado, en el formato correcto.
     * @param sexo El sexo del usuario.
     * @return El primer caracter del sexo.
     */
    String sexo(String sexo){
        sexo = sinAcentos(sexo);
        switch(sexo){
            case "HOMBRE":
                sexo = "H";
                break;
            case "MASCULINO":
                sexo = "H";
                break;
            case "MUJER":
                sexo = "M";
                break;
            case "FEMENINO":
                sexo = "M";
                break;
        }
        return sexo;
    }
    
    /**
     * Metodo que regresa la abreviatura del estado, en el formato correcto.
     * El metodo debe de aceptar entradas en minusculas, mayusculas o mixta,
     * tambien debe de aceptar entradas con acentos y sin acentos.
     * @param estado El estado de nacimiento del usuario.
     * @return La abreviatura del usuario.
     */
    String estado(String estado){
        estado = sinAcentos(estado);
        switch (estado){
            case "AGUASCALIENTES":
                estado = "AS";
                break;
            case "BAJA CALIFORNIA SUR":
                estado = "BS";
                break;
            case "COAHUILA":
                estado ="CL";
                break;
            case "CHIAPAS":
                estado ="CS";
                break;
            case "DISTRITO FEDERAL":
                estado ="DF";
                break;
            case "CIUDAD DE MEXICO":
                estado ="DF";
                break;
            case "GUANAJUATO":
                estado ="GT";
                break;
            case "HIDALGO":
                estado ="HG";
                break;
            case "ESTADO DE MEXICO":
                estado = "MC";
                break;
            case "MEXICO":
                estado ="MC";
                break;
            case "MORELOS":
                estado ="MS";
                break;
            case "NUEVO LEON":
                estado ="NL";
                break;
            case "PUEBLA":
                estado ="PL";
                break;
            case "QUINTANA ROO":
                estado ="QR";
                break;
            case "SINALOA":
                estado ="SL";
                break;
            case "TABASCO":
                estado ="TC";
                break;
            case "TLAXCALA":
                estado ="TL";
                break;
            case "YUCATAN":
                estado ="YN";
                break;
            case "BAJA CALIFRONIA":
                estado = "BC";
                break;
            case "CAMPECHE":
                estado = "CC";
                break;
            case "COLIMA":
                estado = "CM";
                break;
            case "CHIHUAHUA":
                estado = "CH";
                break;
            case "DURANGO":
                estado = "DG";
                break;
            case "GUERRERO":
                estado = "GR";
                break;
            case "JALISCO":
                estado = "JC";
                break;
            case "MICHOACAN":
                estado = "MN";
                break;
            case "NAYARIT":
                estado = "NT";
                break;
            case "OAXACA":
                estado = "OC";
                break;
            case "QUERETARON":
                estado = "QT";
                break;
            case "SAN LUIS POTOSI":
                estado = "SP";
                break;
            case "SONORA":
                estado = "SR";
                break;
            case "TAMAULIPAS":
                estado = "TS";
                break;
            case "VERACRUZ":
                estado = "VZ";
                break;
            case "ZACATECAS":
                estado = "ZS";
                break;
            default:
                estado = "NE";
                break;
            
        }
        return estado;
    }
    
    /**
     * Metodo que regresa la segunda consonante de una cadena.
     * Hint: remplacen las vocales con acentos por vocales sin acentos y
     * pasen toda la cadena a minusculas o mayusculas para que tengan menos casos que revisar.
     * Tengo entendido que no es necesaria la segunda consonante, si no la primera que se encuentre después
     * de la primera letra 
     * @param cadena La cadena dada.
     * @return La segunda consonante de una cadena.
     */
    char getConsonantes(String cadena){
        cadena = sinAcentos(cadena);
        char consonante = ' ';
        int i=1;
        while(consonante == ' '){
            switch(cadena.charAt(i)){
                    case 'A':
                        i++;
                        break;
                    case 'E':
                        i++;
                        break;
                    case 'I':
                        i++;
                        break;
                    case 'O':
                        i++;
                        break;
                    case 'U':
                        i++;
                        break;
                    default:
                        consonante = cadena.charAt(i);
                        break;
                }
        }
    return consonante;  
    }
}